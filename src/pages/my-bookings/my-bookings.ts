import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController, ActionSheetController, ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransferObject, FileUploadOptions, FileTransfer } from '@ionic-native/file-transfer';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-my-bookings',
  templateUrl: 'my-bookings.html',
})
export class MyBookingsPage {
  isLogin: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private apiCall: ApiServiceProvider,
    private toastCtrl: ToastController
  ) {
    this.isLogin = JSON.parse(localStorage.getItem('details')) || {};
  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad MyBookingsPage');
    this.getBooking();
  }

  createBooking() {
    let modal = this.modalCtrl.create(AddBookingModal);
    modal.onDidDismiss(() => {
      this.getBooking();
    })
    modal.present();
  }
  bookingsData: any = [];
  getBooking() {
    let _baseUrl = this.apiCall.mainUrl + "vehicleBooking/getBookingDetail?user_id=" + this.isLogin._id;
    // this.apiCall.startLoading()
    this.apiCall.getdevicesForAllVehiclesApi(_baseUrl).subscribe(data => {
      console.log("check bookings: ", data);
      if (data) {
        this.bookingsData = data;
      }
    },
      err => {
        console.log(err);
        this.toastCtrl.create({
          message: JSON.parse(err._body).message,
          position: 'middle',
          duration: 2000
        }).present();
      })

  }
  showBooking(item) {
    this.navCtrl.push('BookingDetailPage', {
      params: item
    });
  }
}

@Component({
  selector: 'page-my-bookings',
  templateUrl: 'add-booking.html'
})

export class AddBookingModal {
  addBookingForm: FormGroup;
  addVehicleForm: FormGroup;
  submitAttempt: boolean;
  isLogin: any;
  vehicle_name: any;

  constructor(
    public viewCtrl: ViewController,
    public formBuilder: FormBuilder,
    private apiCall: ApiServiceProvider,
    private actionSheetCtrl: ActionSheetController,
    private camera: Camera,
    private transfer: FileTransfer,
    private toastCtrl: ToastController
  ) {
    this.isLogin = JSON.parse(localStorage.getItem('details')) || {};
    this.getdevices();
    this.addBookingForm = formBuilder.group({
      name: ['', Validators.required],
      contract_number: [''],
      tele: [''],
      email: [''],
      dob: [''],
      driver_licence: ['', Validators.required],
      nationality: [''],
      permanent_address: [''],
      dl_VU: ['', Validators.required],
      idcard_passport: ['', Validators.required],
      id_VU: ['', Validators.required]
    });

    this.addVehicleForm = formBuilder.group({
      // vehicle_name: ['', Validators.required],
      brand: ['', Validators.required],
      device_model: ['', Validators.required],
      tuition: [''],
      number: [''],
      home_delivery: [''],
      damage_limit: [''],
      expected_return_date: ['', Validators.required],
      pickup_date: ['', Validators.required],
      priceperday: ['', Validators.required],
      pricepermonth: ['', Validators.required],
      totalprice: ['', Validators.required],
      fuellevel: ['', Validators.required]
    });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
  customer_id: any;
  idcard_passport: any;
  id_VU: any;
  addBooking(key) {
    this.submitAttempt = true;

    let _baseUrl, payload;
    if (key === 'cust') {
      if (this.addBookingForm.valid) {
        // _baseUrl = this.apiCall.mainUrl + "order/addOrderContact";
        _baseUrl = this.apiCall.mainUrl + "order/addOrderContact";
        payload = {
          "customer_full_name": this.addBookingForm.value.name,
          "customer_phone": (this.addBookingForm.value.tele ? this.addBookingForm.value.tele : null),
          "customer_address": (this.addBookingForm.value.permanent_address ? this.addBookingForm.value.permanent_address : null),
          "icard_detail": {
            "icard_number": this.addBookingForm.value.idcard_passport,
            "valid_upto": new Date(this.addBookingForm.value.id_VU).toISOString()
          },
          "date_of_birth": (this.addBookingForm.value.dob ? new Date(this.addBookingForm.value.dob).toISOString() : null),
          "driving_license": {
            "dl_number": this.addBookingForm.value.driver_licence,
            "valid_upto": new Date(this.addBookingForm.value.dl_VU).toISOString()
          },
          "admin": this.isLogin._id,
          "contract_no": (this.addBookingForm.value.contract_number ? this.addBookingForm.value.contract_number : null),
          "email_id": (this.addBookingForm.value.email ? this.addBookingForm.value.email : null),
          "nationality": (this.addBookingForm.value.nationality ? this.addBookingForm.value.nationality : 'Indian')
        }
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(_baseUrl, payload).subscribe(respData => {
          this.apiCall.stopLoading();
          console.log(respData);
          if (!respData.message) {
            this.customer_id = respData._id;
            this.toastCtrl.create({
              message: 'Customer info saved successfully',
              duration: 2000,
              position: 'middle'
            }).present();
            this.addBookingForm.reset();
            this.submitAttempt = false;
            this.showCustForm = false;
            this.showVehicleForm = true;
          }
          if (respData.message) {
            this.toastCtrl.create({
              message: respData.message,
              duration: 2000,
              position: 'bottom'
            }).present();


          }
        },
          err => {
            this.apiCall.stopLoading();
            console.log(err);
          });
      }
    } else if (key === 'vehicle') {
      if (this.addVehicleForm.valid) {
        if (this.customer_id === undefined) {
          this.toastCtrl.create({
            message: 'Please submit customer details first...',
            duration: 2000,
            position: 'middle'
          }).present();
          return;
        }
        _baseUrl = this.apiCall.mainUrl + "vehicleBooking/addBooking";

        payload = {
          "user_id": this.isLogin._id,
          "device": this.vehData._id,
          "customer_id": this.customer_id,
          "pickup_date": new Date(this.addVehicleForm.value.pickup_date).toISOString(),
          "expected_return_date": new Date(this.addVehicleForm.value.expected_return_date).toISOString(),
          "Vehicle_brand": this.addVehicleForm.value.brand,
          "Vehicle_model": this.device_model_id,
          "home_delivery": (this.addVehicleForm.value.home_delivery ? this.addVehicleForm.value.home_delivery : null),
          "damage_limit": (this.addVehicleForm.value.damage_limit ? this.addVehicleForm.value.damage_limit : null),
          "images": this.allData
        }
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(_baseUrl, payload).subscribe(respData => {
          this.apiCall.stopLoading();
          console.log("vehicle booking data: ", respData);
          this.toastCtrl.create({
            message: 'Booking confirmed!!',
            duration: 2000,
            position: 'middle'
          }).present();
          this.addVehicleForm.reset();
          this.submitAttempt = false;
          this.vehicle_name = undefined;
          this.vehData = undefined;
          this.device_model_id = undefined;
          this.allData = [];
        },
          err => {
            this.apiCall.stopLoading();
            console.log("got error: ", err);
          });
      }
    } else if (key === 'custupdate') {
      if (this.addBookingForm.valid) {
        _baseUrl = this.apiCall.mainUrl + "order/updateOrderContact";
        payload = {
          "customer_full_name": this.addBookingForm.value.name,
          "customer_phone": (this.addBookingForm.value.tele ? this.addBookingForm.value.tele : null),
          "customer_address": (this.addBookingForm.value.permanent_address ? this.addBookingForm.value.permanent_address : null),
          "icard_detail": {
            "icard_number": this.addBookingForm.value.idcard_passport,
            "valid_upto": new Date(this.addBookingForm.value.id_VU).toISOString()
          },
          "date_of_birth": (this.addBookingForm.value.dob ? new Date(this.addBookingForm.value.dob).toISOString() : null),
          "driving_license": {
            "dl_number": this.addBookingForm.value.driver_licence,
            "valid_upto": new Date(this.addBookingForm.value.dl_VU).toISOString()
          },
          "admin": this.isLogin._id,
          "contract_no": (this.addBookingForm.value.contract_number ? this.addBookingForm.value.contract_number : null),
          "email_id": (this.addBookingForm.value.email ? this.addBookingForm.value.email : null),
          "nationality": (this.addBookingForm.value.nationality ? this.addBookingForm.value.nationality : 'Indian'),
          "_id": this.customer_id
        }
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(_baseUrl, payload).subscribe(respData => {
          this.apiCall.stopLoading();
          console.log(respData);
          if (!respData.message) {
            this.customer_id = respData._id;
            this.toastCtrl.create({
              message: 'Customer info saved successfully',
              duration: 2000,
              position: 'middle'
            }).present();
            this.addBookingForm.reset();
            this.submitAttempt = false;
            this.showCustForm = false;
            this.showVehicleForm = true;
          }
        },
          err => {
            this.apiCall.stopLoading();
            console.log(err);
          });
      }
    }
  }
  showCustForm: boolean = false;
  showVehicleForm: boolean = false;
  checkBtn(key) {
    if (key === 'cust') {
      this.showCustForm = !this.showCustForm;
      this.showVehicleForm = false;
      this.submitAttempt = false;
    } else if (key === 'vehicle') {
      this.showVehicleForm = !this.showVehicleForm;
      this.showCustForm = false;
      this.submitAttempt = false;
    }
  }

  portstemp: any = [];
  getdevices() {
    var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.isLogin._id + '&email=' + this.isLogin.email;
    if (this.isLogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.isLogin._id;
    } else {
      if (this.isLogin.isDealer == true) {
        baseURLp += '&dealer=' + this.isLogin._id;
      }
    }
    this.apiCall.startLoading().present();
    this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.portstemp = data.devices;
      },
        error => {
          this.apiCall.stopLoading();
          console.log(error);
        });
  }
  device_model_id: any;
  vehData: any;
  onChangedSelect(pdata) {
    this.allData = [];
    this.upload_doc = false;
    console.log(pdata);
    this.addVehicleForm.patchValue({
      device_model: pdata.vehicleType.model,
      brand: pdata.vehicleType.brand
    });
    this.device_model_id = pdata.device_model._id;
    this.vehData = pdata;
    if (this.vehData.imageDoc.length > 0) {
      for (var i = 0; i < this.vehData.imageDoc.length; i++) {
        let gg = this.vehData.imageDoc[i];
        var imgUrl = gg + '';;
        var str1 = imgUrl.split('public/');
        this.allData.push({
          fileUrl: "http://wetrack.pk/" + str1[1]
        });
      }
      console.log("length: ", this.allData.length);
      console.log("allData: " + JSON.stringify(this.allData));
      this.upload_doc = true;
    }
  }

  public selectImage() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.pickImage(this.camera.PictureSourceType.SAVEDPHOTOALBUM);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.pickImage(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  pickImage(sourceType) {
    var url = "http://wetrack.pk/users/uploadImage";


    const options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      // let base64Image = 'data:image/jpeg;base64,' + imageData;
      console.log("imageData: ", imageData)
      debugger
      // this.crop.crop(imageData, { quality: 100 })
      //   .then(
      //     newImage => {
      var dlink123 = imageData.split('?');
      var wear = dlink123[0];
      const fileTransfer: FileTransferObject = this.transfer.create();
      const uploadOpts: FileUploadOptions = {
        fileKey: 'photo',
        // fileName: imageData.substr(imageData.lastIndexOf('/') + 1)
        fileName: wear.substr(wear.lastIndexOf('/') + 1)
      };

      this.apiCall.startLoading().present();
      debugger
      fileTransfer.upload(wear, url, uploadOpts)
        .then((data) => {
          this.apiCall.stopLoading();

          console.log(data);
          // this.selectedFile = <File>event.target.files[0];
          var respData = data.response;
          console.log("image data response: ", respData);
          this.dlUpdate(respData);
          // this.fileUrl = this.respData.fileUrl;
          // this.fileUrl = this.respData;
        }, (err) => {
          this.apiCall.stopLoading();
          console.log(err);
          this.toastCtrl.create({
            message: 'Something went wrong while uploading file... Please try after some time..',
            duration: 2000,
            position: 'bottom'
          }).present();
        });
      // });
    }, (err) => {
      console.log("imageData err: ", err)
      // Handle error
    });
  }

  dlUpdate(dllink) {
    // debugger
    // let that = this;
    var dlink123 = dllink.split('?');
    var wear = dlink123[0];
    console.log("new download link: ", wear);
    // var _burl = this.apiCall.mainUrl + "users/updateImagePath";
    var _burl = "http://wetrack.pk/devices/deviceupdate";
    var payload = {
      imageDoc: [dllink],
      _id: this.vehData._id
    }
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(_burl, payload)
      .subscribe(respData => {
        this.apiCall.stopLoading();
        console.log("check profile upload: ", respData)
        this.getImgUrl();
      },
        err => {
          this.apiCall.stopLoading();
        });
  }
  allData: any = [];
  getImgUrl() {
    this.allData = [];
    // var url = this.apiCall.mainUrl + "users/shareProfileImage?uid=" + this.islogin._id;
    // var url = "http://wetrack.pk/users/shareProfileImage?uid=" + this.isLogin._id;
    var url = 'http://wetrack.pk/devices/getDevicebyId?deviceId=' + this.vehData.Device_ID;
    // this.apiCall.startLoading().present();
    this.apiCall.getdevicesForAllVehiclesApi(url)
      .subscribe(resp => {
        // this.apiCall.stopLoading();
        console.log("server image url=> ", resp);
        if (JSON.parse(JSON.stringify(resp)).imageDoc.length > 0) {
          for (var i = 0; i < JSON.parse(JSON.stringify(resp)).imageDoc.length; i++) {
            let gg = JSON.parse(JSON.stringify(resp)).imageDoc[i];
            var imgUrl = gg + '';;
            var str1 = imgUrl.split('public/');
            this.allData.push({
              fileUrl: "http://wetrack.pk/" + str1[1]
            });
          }
          console.log("length: ", this.allData.length);
          console.log("allData: " + JSON.stringify(this.allData));
        }
      },
        err => {
          // this.apiCall.stopLoading();
        })
      ;
  }
  upload_doc: boolean = false;
  uploadDocCheck() {
    if (this.vehData === undefined) {
      this.toastCtrl.create({
        message: 'Please select the vehicle first...',
        duration: 2000,
        position: 'middle'
      }).present();
      return;
    }
    this.upload_doc = true;
  }

  showBtn: boolean = false;
  showUpdateBtn: boolean = false;
  verifyId() {
    if (this.addBookingForm.value.idcard_passport) {
      let url = this.apiCall.mainUrl + "order/getCustomerwithicard?icard_number=" + this.addBookingForm.value.idcard_passport;
      this.apiCall.getSOSReportAPI(url)
        .subscribe(res => {
          console.log(res);
          debugger
          if (res.message === 'costumer alredy exists') {
            this.showUpdateBtn = true;
            let submittedData = res['costumer object'];
            console.log("data: ", submittedData);
            console.log("slkdsnlsdsaldmas;dma;s ", moment(new Date(submittedData.date_of_birth), 'DD/MM/YYYY').format('YYYY-MM-DD'))
            this.customer_id = submittedData._id;
            this.addBookingForm.patchValue({
              name: submittedData.customer_full_name,
              contract_number: (submittedData.contract_no ? submittedData.contract_no : null),
              tele: (submittedData.customer_phone ? submittedData.customer_phone : null),
              email: (submittedData.email_id ? submittedData.email_id : null),
              dob: (submittedData.date_of_birth ? moment(new Date(submittedData.date_of_birth), 'DD/MM/YYYY').format('YYYY-MM-DD') : null),
              driver_licence: submittedData.driving_license.dl_number,
              nationality: submittedData.nationality,
              permanent_address: (submittedData.customer_address ? submittedData.customer_address : null),
              dl_VU: moment(new Date(submittedData.driving_license.valid_upto), 'DD/MM/YYYY').format('YYYY-MM-DD'),
              idcard_passport: submittedData.icard_detail.icard_number,
              id_VU: moment(new Date(submittedData.icard_detail.valid_upto), 'DD/MM/YYYY').format('YYYY-MM-DD')
            })
          } else if (res.message === 'No customer found') {
            this.showUpdateBtn = false;
            this.addBookingForm.patchValue({
              name: null,
              contract_number: null,
              tele: null,
              email: null,
              dob: null,
              driver_licence: null,
              nationality: null,
              permanent_address: null,
              dl_VU: null,
              idcard_passport: this.addBookingForm.value.idcard_passport,
              id_VU: null
            })
          }
        },
          err => {
            console.log(err);
          })
    }
  }
}
