// import { HttpClient } from '@angular/common/http';
// import { Injectable } from '@angular/core';
// import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

// @Injectable()
// export class CordovaSqLiteProvider {
//   sqldbObj: SQLiteObject;

//   constructor(
//     public http: HttpClient,
//     public sqlite: SQLite
//   ) {
//     console.log('Hello CordovaSqLiteProvider Provider');
//   }

//   creteSQLiteDB() {
//     this.sqlite.create({
//       name: 'oneqlik_vts.db',
//       location: 'default'
//     }).then((db: SQLiteObject) => {
//       return db.executeSql(`
//       CREATE TABLE IF NOT EXISTS vehicle_list(
//         _id VARCHAR, 
//         Device_Name TEXT,
//         Device_ID BIGINT,
//         supAdmin VARCHAR,
//         Dealer VARCHAR,
//         expiration_date VARCHAR, 
//         status_updated_at VARCHAR, 
//         fuel_percent INT, 
//         currentFuel INT,
//         last_speed INT,
//         created_on VARCHAR, 
//         today_odo FLOAT, 
//         contact_number BIGINT, 
//         iconType TEXT,
//         vehicleType TEXT,
//         status TEXT,
//         last_lat FLOAT,
//         last_lng FLOAT)`, [])
//         .then(res => {
//           this.sqldbObj = db;
//           console.log('Executed SQL', res);
//           // this.sqldbObj.executeSql('DROP TABLE vehicle_list;').then(res => {
//           //   console.log('deleted data table: ', res)
//           //   this.creteSQLiteDB();
//           // }).catch(e => {
//           //   this.creteSQLiteDB();
//           //   // this.getVehicleList();
//           //   console.log(e);
//           // })
//           // this.deleteSQLiteDB();

//         })
//         .catch(e => {

//           // this.sqldbObj.executeSql('DROP TABLE vehicle_list;').then(res => {
//           //   console.log('deleted data table: ', res)
//           //   this.creteSQLiteDB();
//           // }).catch(e => {
//           //   this.creteSQLiteDB();
//           //   // this.getVehicleList();
//           //   console.log(e);
//           // })
//           console.log(e)
//         });
//     })
//   }

//   deleteSQLiteDB() {
//     this.sqldbObj.executeSql('DELETE FROM vehicle_list').then(res => {
//       console.log('deleted data table: ', res)
//       // this.getVehicleList();
//     }).catch(e => {
//       // this.creteSQLiteDB();
//       // this.getVehicleList();
//       console.log(e);
//     })
//   }

//   getVehicleDataFromSQLiteDB() {
//     this.sqlite.create({
//       name: 'oneqlik_vts.db',
//       location: 'default'
//     }).then((db: SQLiteObject) => {
//       db.executeSql(`
//       CREATE TABLE IF NOT EXISTS vehicle_list(
//         _id VARCHAR, 
//         Device_Name TEXT,
//         Device_ID BIGINT,
//         supAdmin VARCHAR,
//         Dealer VARCHAR,
//         expiration_date VARCHAR, 
//         status_updated_at VARCHAR, 
//         fuel_percent INT, 
//         currentFuel INT,
//         last_speed INT,
//         created_on VARCHAR, 
//         today_odo FLOAT, 
//         contact_number BIGINT, 
//         iconType TEXT,
//         vehicleType TEXT,
//         status TEXT,
//         last_lat FLOAT,
//         last_lng FLOAT)`, [])
//         .then(res => {
//           db.executeSql('SELECT * FROM vehicle_list', [])
//             .then(res => {
//               if (res.rows.length > 0) {
//                 var temparray = [];
//                 for (var i = 0; i < res.rows.length; i++) {
//                   temparray.push(res.rows.item(i));
//                 }
//               }

//               return temparray;
//             })
//             .catch(e => console.log(e));
//         })
//         .catch(e => {
//           console.log(e)
//         });
//     });
//   }

// }
